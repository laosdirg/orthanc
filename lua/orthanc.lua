function OnStoredInstance(instanceId, tags, metadata)
  -- Delete(SendToModality(instanceId, 'sample'))
  print('OnStoredInstance')
end

function IncomingFindRequestFilter(query, origin)
  -- First display the content of the C-Find query
  -- PrintRecursive(query)
  -- PrintRecursive(origin)
  --
  -- -- Remove the "PrivateCreator" tag from the query
  -- local v = query
  -- v['5555,0010'] = nil
  --
  -- return v

  print("IncomingFindRequestF2ilter")
  PrintRecursive(query)
  PrintRecursive(origin)

  local queryRetrieveLevel = query["0008,0052"]
  local patientName = query["0010,0010"]
  local patientId = query["0010,0020"]

  local requestBody = {}
  requestBody["Level"] = queryRetrieveLevel
  requestBody["Query"] = {}
  requestBody.Query["PatientName"] = patientName
  requestBody.Query["PatientId"] = patientId

  PrintRecursive(requestBody)

  local resultPath = ParseJson(RestApiPost('/modalities/pacs/query', DumpJson(requestBody, true))) ['Path']
  PrintRecursive("Query result: " .. resultPath)

  -- local retrieveBody = {}
  -- retrieveBody["AET"] = "UNICA"
  --
  -- RestApiPost(resultPath .. '/retrieve', "UNICA")

  local queryResult = ParseJson(RestApiGet(resultPath .. '/answers'))
  print("*** QUERYRESULT ***")

  if tablelength(queryResult) > 0 then
    for i=0,tablelength(queryResult) - 1 do
      co = coroutine.create(function()
        print("hej", i)
        RestApiPost(resultPath .. '/answers/' .. i ..'/retrieve', 'ORTHANC')
      end)
      coroutine.resume(co)
    end
    --local answerContent = ParseJson(RestApiGet(resultPath .. '/answers/0/retrieve'))

    PrintRecursive(answerContent)
  else
    print("Nothin found...")
  end

 return query
end

function OutgoingFindRequestFilter(query, modality)
  -- for key, value in pairs(query) do
  --   if value == '*' then
  --     query[key] = ''
  --   end
  -- end
  --
  print("OutgoingFindRequestFilter")
  print("Modality: " .. modality)
  print("Query: ")
  PrintRecursive(query)
  return query


end

function tablelength(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end

-- function IncomingHttpRequestFilter(method, uri, ip, username, httpHeaders)
--   print("IncomingHttpRequestFilter")
--   print("IncomingHttpRequestFilter")
--   print("method: " .. method)
--   print("uri: " .. uri)
--   print("ip: " .. ip)
--   print("username: " .. username)
-- end
